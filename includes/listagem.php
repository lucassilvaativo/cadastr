<?php 

$mensagem = ' ';
if(isset($_GET['status'])) {
    switch ($_GET['status']) {
        case ' success ';
        $mensagem = '<div class="alert alert-success">Ação executada com sucesso!</div>';
        break;

        case ' error ';
        $mensagem = '<div class="alert alert-success">Ação não executada!</div>';
        break;

    }
}


$resultados = '';
foreach ($vagas as $vaga)
{
    $resultados .= '<tr border="1">
                      <td>'.$vaga->id.'</td>
                      <td>'.$vaga->nome.'</td>
                      <td>'.$vaga->sobrenome.'</td>
                      
                      <td>'.$vaga->rua.'</td>
                      <td>'.$vaga->bairro.'</td>
                      <td>'.$vaga->numero.'</td>
                      
                      <td>'.$vaga->estado.'</td>
                      <td>'.$vaga->CEP.'</td>
                      
                      <td>'.($vaga->ativo == 's' ? 'Ativo' : 'Inativo').'</td>
                     
                      <td>'.date('d/m/Y à\s H:i:s',strtotime($vaga->data)).'</td> 
                      <td><img style="width:60px;height:60px;" src='.$vaga->imagem.'></img></td>
                        <td>
                         <a href="editar.php?id='.$vaga->id.'">
                         <button type="button" class="btn btn-primary">Editar
                         </button></a>

                         <a href="excluir.php?id='.$vaga->id.'">
                         <button type="button" class="btn btn-danger">Excluir
                         </button></a>
                         </td>
                      </tr>';
                      
                      
}

?>

<main>

<?=$mensagem?>
   
    <section>
        <a href="cadastrar.php">
            <button class="btn btn-success">Cadastrar</button>
        </a>
    </section>


    <section>
        <table class="table mt-3" border="1">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nome</th>
                    <th>Sobrenome</th>
                    <!-- <th>Email</th> -->
                    <th>Rua</th>
                    <th>Bairro</th>
                    <th>Número</th>
                    <!-- <th>complemento</th> -->
                    <th>Estado</th>
                    <th>CEP</th>
                    <th>Ativo/Inativo</th>
                    <!-- <th>observacao</th> -->
                    <th>Data</th>
                    <th>Imagem</th>
                    <th></th>
                </tr>
            </thead>

            <tbody>
             <?=$resultados?>
            </tbody>
        </table>
    </section>
</main>