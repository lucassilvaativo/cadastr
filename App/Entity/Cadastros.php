<?php 
   
   namespace App\Entity;

use App\DB\Database;

use \PDO;

class Cadastros {

     /**
      * Identificador unico de cadastros
      *  @var integer 
      */

      public $id;

      /**
       * nome do usuario
       * @var string
       */

      public $nome;

       /**
       * sobrenome do usuario
       * @var string
       */

      public $sobrenome;

            /**
       *email do usuario
       *@var string
       */

      public $Email;


            /**
       * rua do usuario
       * @var string
       */

      public $rua;

            /**
       * bairro do usuario
       * @var string
       */

      public $bairro;

            /**
       * número do usuario
       * @var string
       */

      public $numero;

            /**
       * complemento do usuario
       * @var string
       */

      public $complemento;

            /**
       * cidade do usuario
       * @var string
       */

      public $cidade;

            /**
       * estado do usuario
       * @var string
       */

      public $estado;

            /**
       * CEP do usuario
       * @var string
       */

      public $CEP;

            /**
       * ativo marcação do usuario
       * @var string(s/n)
       */

      public $ativo;


            /**
       * Observações do usuario
       * @var string
       */

      public $observacoes;
      

      /**
       * Imagens do usuario
       */
      public $imagem;

     /**
       * data cadastro do usuario
       * @var string
       */

      public $data;

      /**
       * Metodo responsavel por colocar o cadastro no banco
       *  @return boolean
       */

      public function cadastrar()
      {
           // definição data
         $this->data = date('Y-m-d H:i:s');

       // inserir cadastro no banco 
        $obDatabase = new Database ('cadastros');
        $this->id = $obDatabase-> insert ([
                         'nome' => $this->nome,
                         'sobrenome' => $this->sobrenome,
                         'Email' => $this->Email,
                         'rua' => $this->rua,
                         'bairro' => $this->bairro,
                         'numero' => $this->numero,
                         'complemento' => $this->complemento,
                         'cidade' => $this->cidade,
                         'estado' => $this->estado,
                         'CEP' => $this->CEP,
                         'ativo' => $this->ativo,
                         'observacao' => $this->observacoes,
                         'imagem' => $this->imagem,
                         'data' => $this->data
                     ]);

                    //   echo "<pre>"; print_r($this); echo "</pre>"; exit;

       //retornar cadastro com sucesso 
       return true; 
      }

      /**Método responsavel por obter os cadastros do banco de dados
       *  @param string $where
       *  @param string $order
       *  @param string $limit
       *  @return array
       */

      public static function getVagas($where = null , $order = null , $limit = null) {

          return(new Database('cadastros'))->select($where,$order,$limit)
          ->fetchAll(PDO:: FETCH_CLASS,self::class);

      }


      /**Metodo responsavel por buscar um cadastro com base no ID
       *  @param  integer $id
       *  @return Cadastros
       */

      public static function  getVaga($id) {
        return (new Database('cadastros'))->select('id = '.$id )
        ->fetchObject(self::class);
      }

      /** 
       * Metodo por atualizar o banco
       * @return boolean
      */

      public function atualizar () {
         return (new Database('cadastros'))->update('id = '.$this->id , [
                                                       'nome' => $this->nome,
                                                       'sobrenome' => $this->sobrenome,
                                                       'Email' => $this->Email,
                                                       'rua' => $this->rua,
                                                       'bairro' => $this->bairro,
                                                       'numero' => $this->numero,
                                                       'complemento' => $this->complemento,
                                                       'cidade' => $this->cidade,
                                                       'estado' => $this->estado,
                                                       'CEP' => $this->CEP,
                                                       'ativo' => $this->ativo,
                                                       'observacoes' => $this->observacoes,
                                                       'imagem' => $this-> imagem,
                                                       'data' => $this->data
         ]);
      }

      /**
       * Metodo responsavel por excluir a vaga do banco
       * @return boolean
       */

       public function excluirCadastro() {
            return (new Database('cadastros'))->delete('id = ' .$this->id);
       }
      
   }

?>