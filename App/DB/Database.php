<?php 

namespace App\DB;


use \PDO;
use \PDOException;

class Database {
 
    /**
     * Host de conexão com banco de dados 
     * @var string
     */

    const HOST = 'localhost';

    /**
     * Nome do Banco de dados
     * @var string
     */

    const NAME = '0_transfer_zf';

    /**
     * User do banco de dados
     */

     const USER = 'root';

     /**
      * Senha de acesso ao Banco
      */

     const PASS = '';

     /**
      * Nome da table a ser manipulada
      * @var string
      */
     private $table;

      /**
       *private description
       *@var PDO
       */

      private $connection;

      /**
       * Define a table e instancia e conexão
       * @param string $table
       */

      public function __construct($table = null) 
        {
         $this->table = $table;
         $this->setConnection();
        }

      /**
       * Método responsável por criar uma conexão com o banco de dados
       */
      private function setConnection() 
        {

        try{
             $this->connection = new PDO ('mysql:host=' .self::HOST.';dbname=' .self::NAME,self::USER,self::PASS);
             $this->connection->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

        } catch(PDOException $e) {
            die('ERROR:' .$e->getMessage());
          }
        }


        /**Método responsavel por executar querys dentro do banco de dados 
         * @param string $query
         * @param array $params
         * @return PDOStatement
        */

        public function execute($query,$params = []) {
         try {
           $statement = $this->connection->prepare($query);
           $statement->execute($params);
           return $statement;
         }catch(PDOException $e){
           die ('ERROR' .$e->getMessage());
         }
        }

        /**
         * Método responsavel por inserir dados no banco
         * @param array $values [ field => value ]
         * @return integer
         */
      public function insert($values) {

        //  DADOS DA QUERY
        $fields = array_keys($values);

        //  echo "<pre>"; print_r($values); echo "</pre>"; exit;


        // Montar a QUERY
        $query = 'INSERT INTO  '.$this->table.' 
        (nome,
        sobrenome,
        Email,
        rua,
        bairro,
        numero,
        complemento,
        cidade,
        estado,
        CEP,
        ativo,
        observacoes,
        imagem,
        data) 
        
        VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)';  
       
        //Executa o Insert
        $this->execute($query,array_values($values));
        // echo $query; exit;

        //retorna o ID inserido
        return $this->connection->lastInsertId();
      }  

      /** Metodo responsavel por executar a consulta no banco
       *  @param string $where
       *  @param string $order
       *  @param string $limit
       *  @param string $fields
       *  @return PDOStatement
       */

      public function select($where = null , $order = null , $limit = null , $fields = '*')       
      {

        //dados da query
        $where = strlen($where) ? 'WHERE '.$where :' ';
        $order = strlen($order) ? 'ORDER BY '.$order :' ';
        $limit = strlen($limit) ? 'LIMIT '.$limit :' ';


        //monta a query
         $query = 'SELECT '.$fields.' FROM '.$this->table.' '.$where.' '.$order.' '.$limit;

         //executa a query
         return $this->execute($query);
      }

      /**
       * Metodo responsavel por executar atualização no banco
       * @param string $where
       * @param array $values [ field => value ]
       * @return boolean
       */

       public function update($where,$values)        
       {

         // dados da query
         $fields = array_keys($values);

        // monta query
         $query = ' UPDATE ' .$this->table.' SET '.implode('=?,' ,$fields). '=? WHERE ' .$where;
        
         // Executa a query
         $this->execute($query , array_values($values));

         return true;
       }

       /**Metodo responsavel por excluir dados do banco pela Query
        *  @param string $where
        *  @return boolean
        */

        public function delete($where) {
          // monta a Query
          $query = 'DELETE FROM ' .$this->table. ' WHERE ' .$where;

          //Executa Query
          $this->execute($query);

          // Retorna com sucesso
          return true;
        }
}

?>