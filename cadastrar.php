<?php 

require __DIR__.'/vendor/autoload.php';

define('TITLE' ,'Cadastrar Vaga');

use \App\Entity\Cadastros;

// Validações do POST
if(isset($_POST['nome'],
$_POST['sobrenome'],
$_POST['Email'],
$_POST['rua'],
$_POST['bairro'],
$_POST['numero'],
$_POST['complemento'],
$_POST['cidade'],
$_POST['estado'],
$_POST['CEP'],
$_POST['ativo'],
$_POST['imagem'],
$_POST['observacao']))
{

    $obcadastro = new Cadastros;
    
    $obcadastro->nome = $_POST['nome'];
    $obcadastro->sobrenome = $_POST['sobrenome'];
    $obcadastro->Email = $_POST['Email'];
    $obcadastro->rua = $_POST['rua'];
    $obcadastro->bairro = $_POST['bairro'];
    $obcadastro->numero = $_POST['numero'];
    $obcadastro->complemento = $_POST['complemento'];
    $obcadastro->cidade = $_POST['cidade'];
    $obcadastro->estado = $_POST['estado'];
    $obcadastro->CEP = $_POST['CEP'];
    $obcadastro->ativo = $_POST['ativo'];
    $obcadastro->observacoes = $_POST['observacao'];
    $obcadastro->imagem = $_POST['imagem'];
    $obcadastro->cadastrar();

    header('location: index.php?status=success');
    exit;
    
}

include __DIR__.'/includes/header.php';
include __DIR__.'/includes/formulario.php';
include __DIR__.'/includes/footer.php';





?>