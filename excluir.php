<?php 

require __DIR__.'/vendor/autoload.php';



use \App\Entity\Cadastros;

//validação do ID
if(!isset($_GET['id']) or !is_numeric($_GET['id'])) {
 
    header('location: index.php?status=error');
    exit;
}

$obcadastro = Cadastros::getVaga($_GET['id']);
 

//Validar o cadastro
if(!$obcadastro instanceof Cadastros) {
    header('location: index.php?status=error');
    exit;
}

  

// Validações do POST
if(isset($_POST['excluir']))  

{
    $obcadastro->excluirCadastro();
    header('location: index.php?status=success');
    exit;    
}

include __DIR__.'/includes/header.php';
include __DIR__.'/includes/Confirmar-exclusão.php';
include __DIR__.'/includes/values.php';
include __DIR__.'/includes/footer.php';





?>